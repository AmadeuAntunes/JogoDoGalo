﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jogo_do_Galo_Cliente
{
    public partial class Form1 : Form
    {

       
        TcpClient mTcpClient;
        byte[] mRx;
        bool isConnected = false;

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {

                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }
      

        public Form1()
        {
            InitializeComponent();
            TextBoxIP.Text = GetLocalIPAddress();
            TextBoxPorto.Text = "1500";
        }

        private void btnEscutar_Click(object sender, EventArgs e)
        {

            if (!isConnected)
            {
                IPAddress ipaddr;
                int nporto;
                btnEscutar.Text = "Disconnectar";
                if (!IPAddress.TryParse(TextBoxIP.Text, out ipaddr))
                {
                    listBoxStatus.Items.Add("Endreço IP Inválido" + Environment.NewLine);
                    return;
                }

                if (!int.TryParse(TextBoxPorto.Text, out nporto))
                {
                    listBoxStatus.Items.Add("Porto Inválido" + Environment.NewLine);
                    return;
                }

                if (nporto < 1024 || nporto > 65535)
                {
                    listBoxStatus.Items.Add("Porto Inválido" + Environment.NewLine);
                    return;
                }
                mTcpClient = new TcpClient();
                mTcpClient.BeginConnect(ipaddr, nporto, onCompleteConnect, mTcpClient);

                listBoxStatus.Items.Add("A estabelecer conexão " + ipaddr + " " + nporto.ToString());
                isConnected = true;


            }
            else
            {

              
                isConnected = false;
                listBoxStatus.Items.Add("Connecção desligada");
                btnEscutar.Text = "Connectar";

            }
        }


        private void onCompleteConnect(IAsyncResult iar)
        {
            TcpClient tcpc;
            tcpc = (TcpClient)iar.AsyncState;
            try
            {
                tcpc.EndConnect(iar);
            }
            catch (Exception e)
            {
                listBoxStatus.Invoke((MethodInvoker)delegate
                {
                    listBoxStatus.Items.Add("Ligação não estabelecida ");
                    MessageBox.Show(e.Message);
                });
                return;
            }

            listBoxStatus.Invoke((MethodInvoker)delegate
            {
                listBoxStatus.Items.Add("Ligação estabelecida ");
            });
            mRx = new byte[1460];
            tcpc.GetStream().BeginRead(mRx, 0, mRx.Length, OnCompleteRead, mTcpClient);


        }

        private void OnCompleteRead(IAsyncResult iar)
        {
            TcpClient tcpc;
            string strRecv = String.Empty;
            int nReadBytes = 0;
            tcpc = (TcpClient)iar.AsyncState;
            nReadBytes = tcpc.GetStream().EndRead(iar);
            strRecv = ASCIIEncoding.ASCII.GetString(mRx, 0, nReadBytes);

            listBoxStatus.Invoke((MethodInvoker)delegate
            {
                listBoxStatus.Items.Add(strRecv);
                table(strRecv);
            });
            mRx = new byte[1460];
            mTcpClient.GetStream().BeginRead(mRx, 0, mRx.Length, OnCompleteRead, mTcpClient);

        }


        private void table(string msg)
        {
            string[] words = msg.Split(':');

            string btnname = words[1];
            string simbolo = words[0];

            switch (btnname)
            {
                case "1":
                    button1.Text = simbolo;
                    break;
                case "2":
                    button2.Text = simbolo;
                    break;
                case "3":
                    button3.Text = simbolo;
                    break;

                case "4":
                    button4.Text = simbolo;
                    break;

                case "5":
                    button5.Text = simbolo;
                    break;

                case "6":
                    button6.Text = simbolo;
                    break;

                case "7":
                    button7.Text = simbolo;
                    break;
                case "8":
                    button8.Text = simbolo;
                    break;
                case "9":
                    button9.Text = simbolo;
                    break;
            }
        }

            private string enviar(int pos)
        {
            string msg = String.Empty;
            if (radioButton2.Checked)
            {
                msg = "O";
            }
            else if (radioButton1.Checked)
            {
                msg = "X";
            }
            byte[] sendTxt = new byte[1460];
            if (mTcpClient == null)
            {
                listBoxStatus.Items.Add("O cliente não está ligado");

            }


            if (!mTcpClient.Client.Connected)
            {
                listBoxStatus.Items.Add("Perdeu-se a ligação ");

            }
            sendTxt = ASCIIEncoding.ASCII.GetBytes(msg + ":" + pos);
            mTcpClient.GetStream().BeginWrite(sendTxt, 0, sendTxt.Length, OnCompleteWrite, mTcpClient);
            return msg;
        }
        private void OnCompleteWrite(IAsyncResult iar)
        {
            TcpClient tcpc = (TcpClient)iar.AsyncState;
            tcpc.GetStream().EndWrite(iar);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Text = enviar(1);
        }
        private void button2_Click(object sender, EventArgs e)
        {
            button2.Text = enviar(2);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            button3.Text = enviar(3);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            button4.Text = enviar(4);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            button5.Text = enviar(5);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            button6.Text = enviar(6);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            button7.Text = enviar(7);
        }
        private void button8_Click(object sender, EventArgs e)
        {
            button8.Text = enviar(8);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            button9.Text = enviar(9);
        }
    }
}
